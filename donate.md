---
layout: page
title: Donate
---

# Donate

If you want to help me with my stupid work, you can send me some money and I will create a separate page for your supporters names.

### One-time donations

- [PayPal](https://paypal.me/tophackr2)
- [Ya.Money](https://yasobe.ru/na/tophackrPay)
- [QIWI](https://qiwi.me/tophackrpay)

### Monthly donations

- [Patreon](https://www.patreon.com/tophackr)