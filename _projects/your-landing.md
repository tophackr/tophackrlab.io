---
layout: project
title: Your Landing
subtitle: Platform for your site, basic Jekyll and Git
type: Web Layout
date: 2020-01-08
large_button:
  name: Read more
  url: https://medium.com/tophackr/5187e351f905
repo: https://gitlab.com/your-landing/example-1
---