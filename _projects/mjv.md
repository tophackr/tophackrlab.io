---
layout: project
title: MJV
subtitle: Theme for TeamSpeak 3
type: TS Addon
date: 2018-10-02
large_button:
  name: Read more
  url: https://medium.com/tophackr/6a6952e2d949
repo: https://gitlab.com/tophackr/mjv
---