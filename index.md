---
layout: default
---

<div class="row content">
    <div class="col-md-8 text-center">
        <img src="{% link assets/alexandr-musikhin-picture_min.png %}" class="img-fluid avatar">
        <h1>Alexandr Musikhin</h1>
        <p class="subtitle is-4">Part-time Developer</p>
        <p class="subtitle">I make useless projects. I write in HTML, CSS, PHP, a little JS.</p>
        <a class="btn btn-light btn-lg" href="https://medium.com/@tophackr">
            <i class="fas fa-pencil-alt"></i>
            Writing
        </a>
        <a class="btn btn-light btn-lg" href="https://gitlab.com/tophackr">
            <i class="fab fa-gitlab"></i>
            Code
        </a>
        <a class="btn btn-light btn-lg" href="https://instagram.com/tophackr/">
            <i class="fab fa-instagram"></i>
            Photography
        </a>
    </div>
</div>