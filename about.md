---
layout: page
title: About
---

# About

I was born in Tyumen, Russia, and then moved to Anapa. In this city, I learned that the Internet is a big thing and there met with [fyfywka](https://instagram.com/fyfywka/), who introduced me to programming. Now I live in the city of Sochi and do useless projects from which I do not receive money.

### Voice Chats

- [TeamSpeak](ts3server://abc)
- [Discord](https://discord.gg/mDE2WFZ)

### Social links

- [GitLab](https://gitlab.com/tophackr)
- [Instagram](https://instagram.com/tophackr/)
- [Twitter](https://twitter.com/tophackr_com)
- [Unsplash](https://unsplash.com/@tophackr)
- [Telegram](/contact)
- [Email](/contact)
- [Steam](https://steamcommunity.com/id/tophackr/)
- [Twitch](https://www.twitch.tv/tophackr)